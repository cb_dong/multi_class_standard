import os

DFL_DATA_PATH = os.path.join(os.environ['HOME'], 'VisualSearch','cow_eight')
#DFL_DATA_PATH = '/data/home/xirong/VisualSearch3'
DFL_IMG_SIZE = 448
DFL_CONFIG = 'resnet50_focal_%d_f1' % DFL_IMG_SIZE
DFL_DEVICE = 'cuda'

if __name__ == '__main__':
    print('DATA_PATH {}'.format(DFL_DATA_PATH))
    print('CONFIG {}'.format(DFL_CONFIG))
    print('DEVICE {}'.format(DFL_DEVICE))

