import torchvision
import torch.nn as nn
import re
from functools import partial
from timm.models.efficientnet import tf_efficientnet_b7_ns
import torch
from torch.nn.modules.dropout import Dropout
from torch.nn.modules.linear import Linear
from torch.nn.modules.pooling import AdaptiveAvgPool2d


class ResNet(nn.Module):
    def __init__(self, name, out_dim, pre_trained):
        super(ResNet, self).__init__()
        if '50' in name:
            res = torchvision.models.resnet50(pretrained=pre_trained)
        elif '34' in name:
            res = torchvision.models.resnet34(pretrained=pre_trained)
        else:
            raise Exception('invalid ResNet model {}'.format(name))
            
        self.fconv  =  nn.Sequential(*list(res.children())[:4])
        self.layer1 = res.layer1
        self.layer2 = res.layer2
        self.layer3 = res.layer3
        self.layer4 = res.layer4
        self.avgpool= res.avgpool
        self.fc = nn.Sequential(
            nn.Linear(2048,out_dim,bias=True),
            nn.Dropout(0.5),
            )
        
    def forward(self,x):
        x = self.fconv(x)
        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)
        x = self.avgpool(x)
        x = x.view(x.size(0), -1)
        out = self.fc(x)
        return out, x

    
class VGGNet(nn.Module):
    def __init__(self,pre_trained):
        super(VGGNet,self).__init__()
        self.vgg = torchvision.models.vgg19(pretrained=pre_trained)
        self.features = self.vgg.features
        self.avgpool = self.vgg.avgpool
        self.fc = nn.Sequential(
            nn.Linear(25088,4096,True),
            nn.ReLU(inplace=True),
            nn.Dropout(0.5),
            nn.Linear(4096,512,True),
            nn.ReLU(inplace=True),
            nn.Dropout(0.5),
            nn.Linear(512,1,True),          
        )

    def forward(self,x):
        x = self.features(x)
        x = self.avgpool(x)
        x = x.view(x.size(0),-1)
        x = self.fc(x)
        return x
    
    def load_pretrained(self):
        pretrained_dict = self.vgg.state_dict()
        model_dict = self.features.state_dict()
        pretrained_dict = {k: v for k, v in pretrained_dict.items() if k in model_dict}
        model_dict.update(pretrained_dict)
        self.features.load_state_dict(model_dict)
        



"""
模型定义
"""
encoder_params = {
    "tf_efficientnet_b7_ns": {
        "features": 2560,
        "init_op": partial(tf_efficientnet_b7_ns, pretrained=True, drop_path_rate=0.2)
    },
}

class Efficientnet(nn.Module):
    def __init__(self, encoder, out_dim=2,dropout_rate=0.0) -> None:
        super().__init__()
        self.encoder = encoder_params[encoder]["init_op"]()
        self.avg_pool = AdaptiveAvgPool2d((1, 1))
        self.dropout = Dropout(dropout_rate)
        self.fc = Linear(encoder_params[encoder]["features"], out_dim)

    def forward(self, x):
        x = self.encoder.forward_features(x)
        x = self.avg_pool(x).flatten(1)
        x = self.dropout(x)
        out = self.fc(x)
        return out,x

def build_net(config):
    if 'res' in config.net_name:
        net = ResNet(config.net_name, config.class_num, config.pre_trained)
    elif 'vgg' in config.net_name:
        net = VGGNet(config.pre_trained)
    elif 'Efficientnet' in config.net_name:
        net = Efficientnet(encoder="tf_efficientnet_b7_ns",out_dim=config.class_num)
    else:
        #print("add custom model")
        net = None
    return net

    





