source common.ini

rlaunch --cpu=16 --gpu=8 --memory=8192 -- python train.py --data_path $data_path --train_collection $train_collection --val_collection $val_collection --run_id $run_id --config_name $config_name --device $device --overwrite $overwrite

