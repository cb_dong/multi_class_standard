import os
import argparse
import json
import sys
import torch
import torch.nn as nn
from torch.utils.data import DataLoader
import pdb
import numpy as np
import time

from common import DFL_DATA_PATH, DFL_CONFIG, DFL_DEVICE, DFL_IMG_SIZE
from utils import logger, load_config, get_test_paths, collate_function, Progbar, AverageMeter, predict_set
from models import ResNet
from dataset import ImageDataset


def parse_args():
    parser = argparse.ArgumentParser(description='predict')
    parser.add_argument('--data_path', type=str, default=DFL_DATA_PATH,
                        help='path to datasets. (default: %s)' % DFL_DATA_PATH)
    parser.add_argument('--test_collection', type=str, help='testing collection', required=True)
    parser.add_argument('--model_dir', type=str, help='model_dir', required=True)
    parser.add_argument('--device', default=DFL_DEVICE, type=str, help='cuda:n or cpu (default: %s)' % DFL_DEVICE)
    parser.add_argument('--config_name', type=str, default=DFL_CONFIG,help='model configuration file. (default: %s)' % DFL_CONFIG)
    #parser.add_argument('--img_size', default=DFL_IMG_SIZE, type=int, help='resize input image to a given value (default: %d)'%DFL_IMG_SIZE)
    parser.add_argument('--threshold', default=0.5, type=float, help='threshold for being positive (default: 0.5)')
    parser.add_argument('--overwrite', default=0, type=int, help='overwrite existing files (default: 0)')
    
    args = parser.parse_args()
    return args

def main(argv=None):
    opt = parse_args()
    print(json.dumps(vars(opt), indent=4))
    config = load_config('configs.{}'.format(opt.config_name))
    test_params = {
        'data_path': opt.data_path,
        'test_collection': opt.test_collection,
        'model_dir': opt.model_dir,
        'img_size': config.img_size,
        'threshold': opt.threshold
    }

    model_path = os.path.join(opt.data_path, opt.model_dir, 'model.pth.tar')
    test_data_path, pred_dir = get_test_paths(opt)
    res_file = os.path.join(pred_dir, 'results.txt')
    
    if os.path.exists(res_file):
        if opt.overwrite:
            logger.info('%s exists. overwrite' % res_file) 
        else:
            logger.info('%s exists. stop' % res_file)
            sys.exit(0)
    if not os.path.exists(test_data_path):
        logger.error("Test data file {} not found".format(test_data_path))
        exit()
    if not os.path.exists(model_path):
        logger.error("Model {} not found".format(model_path))
        exit()
    if not os.path.exists(pred_dir):
        os.makedirs(pred_dir)

    test_params_file = os.path.join(pred_dir, 'test_params.json')
    with open(test_params_file, 'w') as fp:
        json.dump(test_params, fp, indent=4)

    net = torch.load(model_path, map_location='cpu')
    net = net.to(opt.device)
    logger.info("%s loaded"%model_path)
    
    test_samples = [x.strip() for x in open(test_data_path).readlines() if x.strip()]
    annotations = [(x,0) for x in test_samples]
    test_set = ImageDataset(annotations, img_size=(config.img_size, config.img_size), aug=False, balance=False)
    
    test_loader = DataLoader(
        dataset=test_set,
        num_workers=1,
        batch_size=32,
        pin_memory=True,
        shuffle=False,
        drop_last=False,
        collate_fn=collate_function
    )
    probs, gt_labels, names = predict_set(net, test_loader, {'device':opt.device, 'run_type': 'predict'})
    pred_labels = np.argmax(probs+[1e-6,-1e-6], axis=1)
    positive_scores = probs[:, 1]

    assert (len(names) == len(pred_labels))

    fw = open(res_file, 'w')
    fw.write('\n'.join(['{} {} {}'.format(names[i], pred_labels[i], positive_scores[i]) for i in range(len(names))]))
    fw.close()

    logger.info("Predictions saved to {}".format(res_file))
        
if __name__ == '__main__':
    main()