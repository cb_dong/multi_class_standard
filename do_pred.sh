source common.ini

model_dir=$train_collection/models/$val_collection/$annotation/$config_name/run_$run_id

python predict.py --data_path $data_path --test_collection $test_collection  --model_dir $model_dir --device $device --overwrite $overwrite --config_name $config_name

