import torch
import torch.nn as nn
from scipy.spatial.distance import cdist
import pdb
class BCEFocalLoss(nn.Module):
    def __init__(self, gamma=1, alpha=0.25, reduction="elementwise_mean"):
        super().__init__()
        self.gamma = gamma
        self.alpha = alpha
        self.reduction = reduction

    def forward(self, pt, target):
        alpha = self.alpha
        loss = - alpha * (1 - pt) ** self.gamma * target * torch.log(pt) - \
        (1 - alpha) * pt ** self.gamma * (1 - target) * torch.log(1 - pt)
        if self.reduction == "elementwise_mean":
            loss = torch.mean(loss)
        elif self.reduction == "sum":
            loss = torch.sum(loss)
        return loss


class FocalLoss(nn.Module):

    def __init__(self, gamma=2, alpha=0.25):
        super(FocalLoss, self).__init__()
        self.gamma = gamma
        self.ce = nn.CrossEntropyLoss()
        self.alpha = alpha

    def forward(self, input, target):
        logp = self.ce(input, target)
        p = torch.exp(-logp)
        loss = self.alpha * (1 - p) ** self.gamma * logp * target.long() + \
               (1 - self.alpha) * (p) ** self.gamma * logp * (1 - target.long())
        return loss.mean()

class TripleLoss(nn.Module):
    def __init__(self,margin):
        super(TripleLoss, self).__init__()
        self.margin = margin
        self.size = 0
        self.triple = nn.TripletMarginLoss(margin=1.0, p=2)

    def get_transmat(self,size):
        a = [i for i in range(size)]
        b = [[a[(i+j)%size] for i in range(size)] for j in range(size)]
        return b

    def forward(self,feats):
        self.size = feats.shape[0] // 2
        trans_mat = self.get_transmat(feats.shape[0] // 2)
        
        ix_1 = [i for i in range(feats.shape[0]) if i % 2]
        ix_2 = [i-1 for i in range(feats.shape[0]) if i % 2]
        
        feat1 = feats[ix_1]
        feat2 = feats[ix_2]

        a1 = torch.cat([feat1 for i in range(self.size-1)],dim=0)
        a2 = torch.cat([feat2 for i in range(self.size-1)],dim=0)
        n1 = torch.cat([feat1[trans_mat[i+1]] for i in range(self.size-1)],dim=0)
        n2 = torch.cat([feat2[trans_mat[i+1]] for i in range(self.size-1)],dim=0)

        a = torch.cat([a1,a1,a2,a2],dim=0)
        p = torch.cat([a2,a2,a1,a1],dim=0)
        n = torch.cat([n1,n2,n1,n2],dim=0)

        loss = self.triple(a,p,n)
        if torch.isnan(loss).any():
            pdb.set_trace()
        return loss
        # # feat1_ = feat1.expand((self.size, self.size, feat1.shape[1]))
        # # feat1T = feat1_.permute((1, 0, 2))   
        # # dists_1 = torch.sqrt(torch.sum((feat1_ - feat1T) ** 2, dim=-1))
        # # dists_1 = self.norm(dists_1,minn,maxx)
        # # self.size = feat1.shape[0]
        # # assert feat1.shape[0] == feat2.shape[0]

        # feat1_ = feat1.expand((self.size, self.size, feat1.shape[1]))
        # feat1T = feat1_.permute((1, 0, 2))
        # feat2_ = feat2.expand((self.size, self.size, feat2.shape[1]))
        # feat2T = feat2_.permute((1, 0, 2))

        # dists_1 = torch.sqrt(torch.sum((feat1_ - feat1T) ** 2, dim=-1))
        # dists_2 = torch.sqrt(torch.sum((feat2_ - feat2T) ** 2, dim=-1))

        # dists_1 = self.norm(dists_1,dists_1.min(),dists_1.max())
        # dists_2 = self.norm(dists_2,dists_2.min(),dists_2.max())
        # loss = self.triple(dists_1) + self.triple(dists_2)
        # # dists_p = torch.sqrt(torch.sum((feat1 - feat2) ** 2, dim=-1))
        # # minn = min(dists_1.min(), dists_2.min(), dists_p.min())
        # # maxx = max(dists_1.max(), dists_2.max(), dists_p.max())
        # # dists_1 = self.norm(dists_1,minn,maxx)
        # # dists_2 = self.norm(dists_2,minn,maxx)
        # # dists_p = self.norm(dists_p,minn,maxx)

        # # loss = self.triple(dists_1,dists_p) + self.triple(dists_2,dists_p)
        # if torch.isnan(loss).any():
        #     pdb.set_trace()
        # return loss / (2*self.size*self.size)


    # def norm(self,dists,minn,maxx):
    #     return (dists - minn) / (maxx - minn)

    # def triple(self,dists):
    #     # dists = -dists+self.margin
    #     # eye = -1 * self.margin * torch.eye(self.size)
    #     # eye = eye.to(dists.device)
    #     # dists += eye
    #     # return torch.sum(torch.relu(dists))
    #     # # ix = [i for i in range(self.size)]
    #     # # dists_n = dists_p.expand((self.size, self.size)).T - dists_n + self.margin
    #     # # eye = -1 * self.margin * torch.eye(self.size)
    #     # # eye = eye.to(dists_p.device)
    #     # # dists_n += eye
    #     # # #dists_n[ix, ix] = 0.0
    #     # # return torch.sum(torch.relu(dists_n))



nn.TripletMarginLoss(margin=1.0, p=2)