# CNN based ps detection on the PS-Battles dataset

## Environment

+ Ubuntu 16.04.6 LTS
+ Python 3.5.2


## Modify Paths
 
Change following paths in common.ini
```bash 
data_path, train_collection, val_collection, test_collection, annotation and config_name  
```

## Training

```bash 
bash do_train.sh 
```

## Prediction
```bash
bash do_pred.sh     
```

## Evaluation  
```bash
bash do_eval.sh
```

## Visualization (via gradcam)
```bash
bash do_gradcam.sh
```

You may also directly run as shown in do_xxx.sh
```bash
python xxx.py --xxx xxx ...
```


