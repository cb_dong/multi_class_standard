import torch
import torch.nn as nn
import torchvision
from torch.utils.data import Dataset
import torchvision.transforms as transforms
from torch.autograd import Variable
from PIL import Image
import cv2
import pdb
import numpy as np
import random
from albumentations.augmentations.functional import rot90
from albumentations.pytorch.functional import img_to_tensor
from albumentations import Compose, RandomBrightnessContrast, \
    HorizontalFlip, FancyPCA, HueSaturationValue, OneOf, ToGray, \
    ShiftScaleRotate, ImageCompression, PadIfNeeded, GaussNoise, GaussianBlur
from transform import IsotropicResize
class ImageDataset(Dataset):
    def __init__(self, annotations, class_num, img_size=(224,224), aug=True, batch_size=64, train=True):
        self.img_size = img_size
        self.class_num = class_num
        self.class_list = [i for i in range(self.class_num)]
        self.batch_num = min(batch_size,self.class_num)
        self.aug = aug
        self.train = train
        if train:
            self.data = [[x for x in annotations if x[1] == lab] for lab in self.class_list]
        else:
            self.data = [annotations]

        self.normalize={"mean": [0.485, 0.456, 0.406],
                        "std": [0.229, 0.224, 0.225]}
        if self.train:
            self.aug_transform = Compose([
                ImageCompression(quality_lower=60, quality_upper=100, p=0.5),
                GaussNoise(p=0.1),
                GaussianBlur(blur_limit=3, p=0.05),
                HorizontalFlip(),
                OneOf([
                    IsotropicResize(max_side=img_size[0], interpolation_down=cv2.INTER_AREA, interpolation_up=cv2.INTER_CUBIC),
                    IsotropicResize(max_side=img_size[0], interpolation_down=cv2.INTER_AREA, interpolation_up=cv2.INTER_LINEAR),
                    IsotropicResize(max_side=img_size[0], interpolation_down=cv2.INTER_LINEAR, interpolation_up=cv2.INTER_LINEAR),
                ], p=1),
                PadIfNeeded(min_height=img_size[0], min_width=img_size[0], border_mode=cv2.BORDER_CONSTANT),
                OneOf([RandomBrightnessContrast(), FancyPCA(), HueSaturationValue()], p=0.7),
                ToGray(p=0.2),
                ShiftScaleRotate(shift_limit=0.1, scale_limit=0.2, rotate_limit=10, border_mode=cv2.BORDER_CONSTANT, p=0.5),
            ])
        else:
            self.aug_transform = Compose([
                IsotropicResize(max_side=img_size[0], interpolation_down=cv2.INTER_AREA, interpolation_up=cv2.INTER_CUBIC),
                PadIfNeeded(min_height=img_size[0], min_width=img_size[0], border_mode=cv2.BORDER_CONSTANT),
                ])

    def __len__(self):
        return max([len(subset) for subset in self.data])

    def __getitem__(self, index):
        if self.train:
            imgs = []
            paths = []
            labs = []
            
            cata = random.sample(self.class_list, self.batch_num)
            for each_class in cata:
                for muti in range(2):
                    safe_idx = (index+muti) % len(self.data[each_class])
                    img_path= self.data[each_class][safe_idx][0]
                    img = self.load_sample(img_path)
                    lab = self.data[each_class][safe_idx][1]

                    imgs.append(img.unsqueeze(0))
                    labs.append(lab)
                    paths.append(img_path)
            return torch.tensor(labs), torch.cat(imgs), labs
        else:
            lab = self.data[0][index][1]
            img_path = self.data[0][index][0]
            img = self.load_sample(img_path)
            lab = torch.tensor(lab,dtype=torch.long)
            return lab, img, img_path
   
    def load_sample(self, img_path):
        try:
            img = cv2.imread(img_path, cv2.IMREAD_COLOR)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            data = self.aug_transform(image=img)
            img = data["image"]
            img = img_to_tensor(img, self.normalize)
        except:
            #pdb.set_trace()
            print(img_path)
            img = torch.randn((3,448,448))
        return img
